from dataclasses import dataclass
import os
import sys
from pathlib import Path
import tarfile
import tempfile
from urllib.request import urlretrieve

import pytest

tests_dir = Path(__file__).parent.resolve()
sys.path.insert(0, str(tests_dir))

@dataclass
class Entry:
    name: str
    path: Path

def pytest_addoption(parser: pytest.Parser):
    parser.addoption("--xkb-config-root", type=Path, help="XKB_CONFIG_ROOT")
    parser.addoption("--xkb-config-roots", type=Path, help="XKB_CONFIG_ROOTs")

def pytest_configure(config: pytest.Config):
    if root := config.getoption("xkb_config_root"):
        if not root.is_dir():
            raise ValueError(f"Invalid XKB_CONFIG_ROOT: {root}")
        pytest.xkb_bases = (root,)
    elif roots := config.getoption("xkb_config_roots"):
        if not roots.is_dir():
            raise ValueError(f"Invalid XKB_CONFIG_ROOTs: {roots}")
        xkb_bases = tuple(
            Entry(path.name, path / "share" / "X11" / "xkb")
            for xkb_root in os.scandir(roots)
            if xkb_root.is_dir()
            and (path := Path(xkb_root.path))
        )
        assert xkb_bases
        pytest.xkb_bases = xkb_bases
    else:
        raise ValueError("No XKB_CONFIG_ROOT defined")

def pytest_generate_tests(metafunc: pytest.Metafunc):
    if "xkb_base" in metafunc.fixturenames:
        values: tuple[Path,...] = tuple(x.path for x in pytest.xkb_bases)
        ids: tuple[str,...] = tuple(x.name for x in pytest.xkb_bases)
        metafunc.parametrize("xkb_base", values, ids=ids, scope="package")