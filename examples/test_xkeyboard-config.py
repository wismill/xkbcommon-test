# SPDX-License-Identifier: MIT

from __future__ import annotations

from functools import reduce
from pathlib import Path
from typing import Optional

import pytest
from xkbcommon_test.lib import *

###############################################################################
# Regression Tests
###############################################################################


# https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/issues/382
@pytest.mark.parametrize("layout,variant,options", [("us", "intl", "lv3:lwin_switch")])
class TestIssue382:
    @pytest.mark.parametrize("mod_key", ("RALT", "LWIN"))
    def test_LevelThree(self, keymap: Keymap, mod_key: str):
        """Both RALT and LWIN are LevelThree modifiers"""
        with keymap.key_down(mod_key):
            r = keymap.tap_and_check("AD01", "adiaeresis", level=3)
            assert r.active_mods == Mod5 == r.consumed_mods
            with keymap.key_down("LFSH"):
                r = keymap.tap_and_check("AD01", "Adiaeresis", level=4)
                assert r.active_mods == Shift | Mod5 == r.consumed_mods

    def test_ShiftAlt(self, keymap: Keymap):
        """LALT+LFSH works as if there was no option"""
        r = keymap.tap_and_check("AC10", "semicolon", level=1)
        assert r.active_mods == NoModifier
        with keymap.key_down("LFSH", "LALT"):
            r = keymap.tap_and_check("AC10", "colon", level=2)
            assert r.active_mods == Shift | Mod1
            assert r.consumed_mods == Shift


# https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/issues/90
# https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/issues/346
class TestIssues90And346:
    @pytest.mark.parametrize(
        "layout,key,keysyms",
        [
            ("fi,us", "TLDE", ("section", "grave")),
            ("dk,us", "TLDE", ("onehalf", "grave")),
            ("fi,us,dk", "TLDE", ("section", "grave", "onehalf")),
        ],
    )
    @pytest.mark.parametrize(
        "options,mod_key,mod",
        [
            ("grp:win_space_toggle", "LWIN", Mod4),
            ("grp:alt_space_toggle", "LALT", Mod1),
        ],
    )
    def test_group_switch_on_all_groups(
        self,
        keymap: Keymap,
        mod_key: str,
        mod: ModifierMask,
        key: str,
        keysyms: tuple[str],
    ):
        """LWIN/LALT + SPCE is a group switch on multiple groups"""
        for group, keysym in enumerate(keysyms, start=1):
            print(group, keysym)
            keymap.tap_and_check(key, keysym, group=group)
            self.switch_group(keymap, mod_key, mod, group % len(keysyms) + 1)
        # Check the group wraps
        keymap.tap_and_check(key, keysyms[0], group=1)

    @staticmethod
    def switch_group(keymap: Keymap, mod_key: str, mod: ModifierMask, group: int):
        with keymap.key_down(mod_key) as r:
            assert r.group == 1  # only defined on first group
            r = keymap.tap_and_check("SPCE", "ISO_Next_Group", group=group, level=2)
            assert r.active_mods == mod == r.consumed_mods


# https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/issues/383
@pytest.mark.parametrize("layout", ["us,ru"])
@pytest.mark.parametrize(
    "options,mod_key,mod",
    [
        ("misc:typo,grp:win_space_toggle,lv3:ralt_switch", "LWIN", Mod4),
        ("misc:typo,grp:alt_space_toggle,lv3:ralt_switch", "LALT", Mod1),
    ],
)
class TestIssue383:
    def test_group_switch(self, keymap: Keymap, mod_key: str, mod: ModifierMask):
        """LWIN + SPCE is a group switch on both groups"""
        # Start with us layout
        self.check_keysyms(keymap, 1, "AC01", "a", "combining_acute")
        # Switch to ru layout
        self.switch_group(keymap, mod_key, mod, 2)
        self.check_keysyms(keymap, 2, "AC01", "Cyrillic_ef", "combining_acute")
        # Switch back to us layout
        self.switch_group(keymap, mod_key, mod, 1)
        self.check_keysyms(keymap, 1, "AC01", "a", "combining_acute")

    @staticmethod
    def switch_group(keymap: Keymap, mod_key: str, mod: ModifierMask, group: int):
        with keymap.key_down(mod_key) as r:
            assert r.group == 1  # only defined on first group
            r = keymap.tap_and_check("SPCE", "ISO_Next_Group", group=group, level=2)
            assert r.active_mods == mod == r.consumed_mods

    @staticmethod
    def check_keysyms(
        keymap: Keymap, group: int, key: str, base_keysym: str, typo_keysym: str
    ):
        # Base keysym
        keymap.tap_and_check(key, base_keysym, group=group, level=1)
        # typo keysym
        with keymap.key_down("RALT") as r:
            assert r.group == 1  # only defined on first group
            r = keymap.tap_and_check(key, typo_keysym, group=group, level=3)
            assert r.active_mods == Mod5 == r.consumed_mods
