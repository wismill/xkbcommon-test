# SPDX-FileCopyrightText: 2024-present Pierre Le Marre <dev@wismill.eu>
#
# SPDX-License-Identifier: MIT
__version__ = "0.0.1"
