import argparse
from pathlib import Path
import subprocess
import sys
import tarfile
import tempfile
from urllib.request import urlretrieve

tests_dir = Path(__file__).parent.resolve()
sys.path.insert(0, str(tests_dir))

URL_TEMPLATE = "https://xorg.freedesktop.org/archive/individual/data/{name}/{name}-{version}.tar.xz"
DIR_TEMPLATE = "{name}-{version}"

parser = argparse.ArgumentParser()
parser.add_argument("root", type=Path, help="XKB_CONFIG_ROOT")
parser.add_argument("version", type=str, nargs="+", help="Versions of xkeyboard-config to test")

args = parser.parse_args()

root = args.root

if not root.is_dir():
    raise ValueError(f"Invalid XKB_CONFIG_ROOT: {root}")
for version in args.version:
    directory = DIR_TEMPLATE.format(name="xkeyboard-config", version=version)
    path: Path = root / directory
    if not path.is_dir():
        url = URL_TEMPLATE.format(name="xkeyboard-config", version=version)
        archive_path: Path = path.with_name(f"{directory}.tar.xz")
        if not archive_path.is_file():
            print(f"Downloading: {url} to: {archive_path}…")
            local_filename, _ = urlretrieve(url, archive_path)
            assert local_filename == archive_path
            print("Download complete.")
        with tempfile.TemporaryDirectory() as tmp:
            print(f"Extracting {archive_path} to: {tmp}…")
            with tarfile.open(archive_path) as f:
                f.extractall(tmp, filter='data')
            print("Extraction completed.")
            print("Building…")
            xkb_data_path = Path(tmp) / directory
            print(f"{xkb_data_path=}")
            command: tuple[str,...] = ("meson", "setup", "build", f"-Dprefix={path}")
            subprocess.run(command , cwd=xkb_data_path)
            command = ("meson", "compile", "-C", "build")
            subprocess.run(command , cwd=xkb_data_path)
            print("Built.")
            print(f"Installing xkeyboard-config in {path}…")
            command = ("meson", "install", "-C", "build")
            subprocess.run(command , cwd=xkb_data_path)
            print("Installed.")