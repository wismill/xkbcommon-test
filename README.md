# xkbcommon-test

[![PyPI - Version](https://img.shields.io/pypi/v/xkbcommon-test.svg)](https://pypi.org/project/xkbcommon-test)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/xkbcommon-test.svg)](https://pypi.org/project/xkbcommon-test)

-----

Test XKB using xkbcommon and Python.

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```bash
python -m venv venv
. venv/bin/activate
pip install xkbcommon-test -e .
```

## Examples

See `src/xkbcommon_test/lib.py` for the features available in tests.

See `examples/test_xkeyboard-config.py` for examples.

```bash
# Activate venv
. venv/bin/activate
# Install meson & pytest
pip install meson pytest
# First setup xkeyboard-config versions
mkdir /tmp/xkb
python -m xkbcommon_test.xkb /tmp/xkb 2.41 2.40 2.35.1
# Then run the tests
cd example
python -m pytest --xkb-config-roots /tmp/xkb . 
```

## License

`xkbcommon-test` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
